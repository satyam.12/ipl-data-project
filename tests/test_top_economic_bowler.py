from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from top_economic_bowler import economy_of_bowlers
from top_economic_bowler import get_top_economic_bowlers
from main import extract_matches
from main import extract_deliveries


class TestTopEconomicBowler(unittest.TestCase):

    def test_economy_of_bowlers(self):
        mock_matches = extract_matches('mock_matches.csv')
        mock_deliveries = extract_deliveries('mock_deliveries.csv')
        year = '2008'
        output = economy_of_bowlers(mock_matches,mock_deliveries,year)
        expected_output = {'P Kumar': 0.75, 'Z Khan': 18.0}
        self.assertEqual(output, expected_output)
        year = '2015'
        output = economy_of_bowlers(mock_matches,mock_deliveries,year)
        expected_output = {'DJ Muthuswami': 6.0,
                            'JA Morkel': 12.0,
                            'M Morkel': 6.75,
                            'NM Coulter-Nile': 8.57,
                            'UT Yadav': 7.5}
        self.assertEqual(output, expected_output)
        year = '2012'
        output = economy_of_bowlers(mock_matches,mock_deliveries,year)
        expected_output = {}
        self.assertEqual(output, expected_output)
        year = '2017'
        output = economy_of_bowlers(mock_matches,mock_deliveries,year)
        expected_output = {'TS Mills': 6.0, 'A Choudhary': 16.0}
        self.assertEqual(output, expected_output)

    def test_get_top_economic_bowlers(self):
        mock_matches = extract_matches('./mock_matches.csv')
        mock_deliveries = extract_deliveries('./mock_deliveries.csv')
        year = '2008'
        rate = economy_of_bowlers(mock_matches,mock_deliveries,year)
        top_bowlers = get_top_economic_bowlers(rate)
        expected_output = (['P Kumar', 'Z Khan'], [0.75, 18.0])
        self.assertEqual(top_bowlers,expected_output)
        year = '2012'
        rate = economy_of_bowlers(mock_matches,mock_deliveries,year)
        top_bowlers = get_top_economic_bowlers(rate)
        expected_output = ([], [])
        self.assertEqual(top_bowlers,expected_output)
        year = '2010'
        rate = economy_of_bowlers(mock_matches,mock_deliveries,year)
        top_bowlers = get_top_economic_bowlers(rate)
        expected_output = (['WPUJC Vaas', 'RP Singh'], [2.67, 5.0])
        self.assertEqual(top_bowlers,expected_output)



if __name__ == '__main__':
    unittest.main()