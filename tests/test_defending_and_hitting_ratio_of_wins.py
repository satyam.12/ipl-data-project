from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from defending_and_hitting_ratio_of_wins import defending_or_hitting_winner
from defending_and_hitting_ratio_of_wins import create_initials
from main import extract_matches


class TestDefendingAndHittingRatio(unittest.TestCase):

    def test_defending_or_hitting_winner(self):
        mock_matches = extract_matches('mock_matches.csv')
        output = defending_or_hitting_winner(mock_matches)
        expected_output = {'Chennai Super Kings': {'Defending': 2, 'Hitting': 0},
                            'Kolkata Knight Riders': {'Defending': 2, 'Hitting': 2},
                            'Mumbai Indians': {'Defending': 1, 'Hitting': 0},
                            'Rajasthan Royals': {'Defending': 0, 'Hitting': 1},
                            'Rising Pune Supergiant': {'Defending': 0, 'Hitting': 2},
                            'Sunrisers Hyderabad': {'Defending': 1, 'Hitting': 0}}
        self.assertEqual(output, expected_output)


    def test_create_initials(self):
        mock_matches = extract_matches('mock_matches.csv')
        teams = defending_or_hitting_winner(mock_matches)
        output = create_initials(teams)
        expected_initials = ['SH','KKR','MI','CSK','RR','RPS']
        self.assertEqual(output,expected_initials)


if __name__ == '__main__':
    unittest.main()