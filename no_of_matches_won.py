from collections import Counter
import matplotlib.pyplot as plt


def no_of_matches_won(matches):
    """compute the number of matches won in every year"""
    teams = return_teams(matches)
    years = return_years(matches)
    winner = {k: [] for k in teams}
    for match in matches:
        for team in teams:
            if team in match['winner']:
                winner[team].append(match['season'])
    winner = {team: dict(Counter(winner[team])) for team in teams}
    for team in winner:
        for year in years:
            if year not in winner[team].keys():
                winner[team][year] = 0
    sorted_winner = {}
    for team in winner:
        temp = {}
        for key in sorted(winner[team].keys()):
            temp[key] = winner[team][key]
        sorted_winner[team] = temp
    del sorted_winner['Rising Pune Supergiants']
    # because in our data we have 2 teams with same name so removing 1
    teams.remove('Rising Pune Supergiants')
    return sorted_winner, teams


def return_years(matches):
    """function to get the years from data"""
    years = []
    for match in matches:
        if match['season'] not in years:
            years.append(match['season'])
    return sorted(years)


def return_teams(matches):
    """function to get the team names from data"""
    names = []
    for match in matches:
        if match['winner'] not in names and match['winner'] != '':
            names.append(match['winner'])
    return names


def plot_no_of_matches_won(sorted_winner, teams, matches):
    """stack plot the number of matches played in every year"""
    plt.subplots(figsize=(10, 10))
    bottom = [0 for i in range(len(return_years(matches)))]
    team_initials = create_initials(teams)
    for team in sorted_winner:
        years = sorted_winner[team]
        win_count = sorted_winner[team]
        axis_x = list(years.keys())
        axis_y = list(win_count.values())
        plt.bar(axis_x, axis_y, bottom=bottom)
        bottom = [a + b for a, b in zip(axis_y, bottom)]
    plt.legend(team_initials, ncol=3, loc=1)
    plt.xlabel('Seasons')
    plt.ylabel('Winnings')
    plt.title("No of Wins per Season")
    plt.xticks(rotation=65)
    plt.savefig('no_of_matches_won.png')
    plt.show()


def create_initials(teams):
    """function to return team initials"""
    initial_of_teams = []
    for team in teams:
        initials = ''
        temp = team.split()
        for part in temp:
            initials += part[0]
        initial_of_teams.append(initials)
    return initial_of_teams


def compute_and_plot_no_of_wins(matches):
    """calling compute and plot functions"""
    winner, teams = no_of_matches_won(matches)
    plot_no_of_matches_won(winner, teams, matches)
