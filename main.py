from matches_played_per_year import compute_and_plot_matches_played_per_year
from no_of_matches_won import compute_and_plot_no_of_wins
from extra_runs_conceded import compute_and_plot_extra_runs_conceded
from top_economic_bowler import compute_and_plot_top_economic_bowlers
from defending_and_hitting_ratio_of_wins import compute_and_plot_defending_hitting_winners
import csv

def extract_matches(name):
    """extrach csv and return data"""
    data = open(name,'r')
    matches = csv.DictReader(data)
    return list(matches)


def extract_deliveries(name):
    """extrach csv and return data"""
    file = open(name,'r')
    deliveries = csv.DictReader(file)
    return list(deliveries)

def main():
    """ Main function to call compute and plot from each module."""
    matches = extract_matches('matches.csv')
    deliveries = extract_deliveries('deliveries.csv')
    compute_and_plot_matches_played_per_year(matches)
    compute_and_plot_no_of_wins(matches)
    compute_and_plot_extra_runs_conceded(matches,deliveries,'2016')
    compute_and_plot_top_economic_bowlers(matches,deliveries,'2015')
    compute_and_plot_defending_hitting_winners(matches)




if __name__ == "__main__":
    """calling main function"""
    main()