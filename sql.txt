--creating database

create database ipl_test_db;

--connecting to ipl_test_db

\c ipl_test_db;

--creating table matches

create table matches (
  id int,
  season int,
  city varchar,
  date date,
  team1 varchar,
  team2 varchar,
  toss_winner varchar,
  toss_decision varchar,
  result varchar,
  dl_applied bool,
  winner varchar,
  win_by_runs int,
  win_by_wickets int,
  player_of_match varchar,
  venue varchar,
  umpire1 varchar,
  umpire2 varchar,
  umpire3 varchar
);

--dumping data to matches

copy matches
from '/home/darkside/Desktop/ipl_boilerplate/matches.csv'
delimiter ',' csv header;

--creating table deliveries

create table deliveries(
  match_id int,
  inning int,
  batting_team varchar,
  bowling_team varchar,
  over int,
  ball int,
  batsman varchar,
  non_striker varchar,
  bowler varchar,
  is_super_over bool,
  wide_runs int,
  bye_runs int,
  legbye_runs int,
  noball_runs int,
  penalty_runs int,
  batsman_runs int,
  extra_runs int,
  total_runs int,
  player_dismissed varchar,
  dismissal_kind varchar,
  fielder varchar
);

--dumping data to deliveries

copy deliveries
from '/home/darkside/Desktop/ipl_boilerplate/deliveries.csv'
delimiter ',' csv header;

--quering no of matches per season

select
  distinct(season),
  count(season)
from matches
group by
  season;

--quering no of wins per season

select
  winner,
  season,
  count(winner)
from matches
group by
  winner,
  season
order by
  winner,
  season;

--quering extra runs conceded for year 2016

select
  deliveries.bowling_team,
  sum(deliveries.extra_runs)
from deliveries
join matches on matches.id = deliveries.match_id
where
  season = 2016
group by
  deliveries.bowling_team;

--quering top economic bowlers

select
  temp.bowler,
  round(
    sum(
      (temp.total_runs * 6.0) / (temp.balls - (temp.wideballs + temp.noballs))
    ),
    2
  ) as economy_rate
from (
    select
      deliveries.bowler,
      sum(
        deliveries.total_runs - (deliveries.bye_runs + deliveries.legbye_runs)
      ) as total_runs,
      sum(
        case
          when deliveries.wide_runs <> 0 then 1
          else 0
        end
      ) as wideballs,
      sum(
        case
          when deliveries.noball_runs <> 0 then 1
          else 0
        end
      ) as noballs,
      count(deliveries.ball) as balls
    from deliveries
    join matches on matches.id = deliveries.match_id
    where
      matches.season = 2015
      and deliveries.is_super_over = False
    group by
      deliveries.bowler
    having
      count(deliveries.ball) >= 12
  ) temp
group by
  temp.bowler
order by
  economy_rate
limit
  10;

--quering the hitting or defending ratio of total wins

select
  winner,
  sum(
    case
      when win_by_runs <> 0 then 1
      else 0
    end
  ) as Hitting,
  sum(
    case
      when win_by_wickets <> 0 then 1
      else 0
    end
  ) as Defending,
  count(winner) as Total_wins
from matches
group by
  winner;
