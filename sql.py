from simplecrypt import decrypt,DecryptionException
from base64 import b64decode
from configparser import ConfigParser
import psycopg2 as pg2


def decryption(secret_key,encrypted_string):
    """
    Takes the secret key & encrypted password from config file and
    returns the decrypted password.
    """
    decrypted = encrypted_string.encode('utf-8')
    decrypted = b64decode(decrypted)
    try:
        decrypted = decrypt(secret_key,decrypted).decode('utf-8')
        return decrypted
    except DecryptionException:
        return None


def connection():
    """
    Function to establish the connection with postgres database
    """
    config = ConfigParser()
    config.read('configuration.ini')
    connection = pg2.connect(
        database = config['Database']['database_name'],
        user = config['Database']['user'],
        password = decryption(config['Crypto']['secret_key'],config['Database']['password']),
        host = config['Database']['host'],
        port = config['Database']['port'])
    cursor = connection.cursor()
    return cursor


def execute_query(query,cursor=connection()):
    """
    Function to execute the SQL Quaries
    """
    cursor.execute(query)
    result = cursor.fetchall()
    return result


def matches_played_per_year(test=False):
    """
    Function to calculate matches playes per year,
    has default value of test argument as False,
    make it True if testing
    """
    matches_per_year = {}
    if test:
        table = 'mock_matches'
    else:
        table = 'matches'
    query = f"""
            select distinct(season), count(season)
            from {table}
            group by season;
            """
    result = execute_query(query)
    for season, wins in result:
        matches_per_year[season] = wins
    return matches_per_year


def no_of_matches_won(test=False):
    """
    Function to calculate number of matches won,
    has default value of test argument as False,
    make it True if testing
    """
    if test:
        table = 'mock_matches'
    else:
        table = 'matches'
    query = f"""
            select winner, season, count(winner)
            from {table}
            group by winner, season
            order by winner, season;
            """
    result = execute_query(query)
    wins, seasons = {}, set()
    for row in result:
        winner, season, winnings = row[0], row[1], row[2]
        if winner is not None:
            seasons.add(season)
            if winner in wins:
                wins[winner][season] = winnings
            elif winner == 'Rising Pune Supergiants':  # modefying it for double entry of 'Rising Pune Supergiants'
                wins['Rising Pune Supergiant'][season] = winnings
            else:
                temp = {season: winnings}
                wins[winner] = temp
    for season in seasons:
        for team in wins.values():
            if season not in team:
                team[season] = 0
    return wins


def defending_hitting_ratio(test=False):
    """
    Function to calculate defending & hitting ratio,
    has default value of test argument as False,
    make it True if testing
    """
    if test:
        table = 'mock_matches'
    else:
        table = 'matches'
    query = f"""
            select winner, sum( case
			                    when win_by_runs <> 0 then 1
                      			else 0 end) as Hitting,
		                    sum( case
			                    when win_by_wickets <> 0 then 1
			                    else 0 end) as Defending,
		                    count(winner) as Total_wins
            from {table}
            group by winner;
            """
    result = execute_query(query)
    winners = {}
    for row in result:
        winner, Hitting, Defending, Total_wins = row[0], row[1], row[2], row[3]
        if winner is not None:
            if winner == 'Rising Pune Supergiant':  # handling duplicate entry for Rising Pune Supergiant'
                winners['Rising Pune Supergiants']['Hitting'] += Hitting
                winners['Rising Pune Supergiants']['Defending'] += Defending
                winners['Rising Pune Supergiants']['Total Wins'] += Total_wins
            elif winner not in winners:
                temp = {
                    'Hitting': Hitting,
                    'Defending': Defending,
                    'Total Wins': Total_wins}
                winners[winner] = temp
            else:
                winners[winner]['Hitting'] += Hitting
                winners[winner]['Defending'] += Defending
                winners[winner]['Total Wins'] += Total_wins
    return winners


def extra_runs_conceded(year,test=False):
    """
    Function to calculate extra runs conceded,
    has default value of test argument as False,
    make it True if testing
    """
    if test:
        table1,table2 = 'mock_matches','mock_deliveries'
    else:
        table1,table2 = 'matches','deliveries'
    query = f"""
            select {table2}.bowling_team, sum({table2}.extra_runs)
            from {table2}
            join {table1} on {table1}.id = {table2}.match_id
            where season = {year}
            group by {table2}.bowling_team;
            """
    result = execute_query(query)
    extras_by_team = {}
    for team, extras in result:
        extras_by_team[team] = extras
    return extras_by_team


def top_economic_bowler(year,cutoff=6,test=False):
    """
    Function to calculate top economic bowlers,
    has default value of test argument as False,
    make it True if testing, and cutoff is set to 1 over,
    i.e. 6 balls
    """
    if test:
        table1,table2 = 'mock_matches','mock_deliveries'
    else:
        table1,table2 = 'matches','deliveries'
    query = f"""
            select temp.bowler,round(sum((temp.total_runs * 6.0) / (temp.balls - (temp.wideballs + temp.noballs))),2) as economy_rate
            from (select {table2}.bowler, sum({table2}.total_runs - ({table2}.bye_runs + {table2}.legbye_runs)) as total_runs,
                  sum(case when {table2}.wide_runs <> 0 then 1 else 0 end) as wideballs,
                  sum(case when {table2}.noball_runs <> 0 then 1 else 0 end) as noballs,
                  count({table2}.ball) as balls
                  from {table2}
                  join {table1} on {table1}.id = {table2}.match_id
                  where {table1}.season = {year} and {table2}.is_super_over = False
                  group by {table2}.bowler
                  having count({table2}.ball) >= {cutoff}) temp
            group by temp.bowler
            order by economy_rate
            limit 10;
            """
    result = execute_query(query)
    economic_bowlers = {}
    for bowler, rate in result:
        economic_bowlers[bowler] = float(rate)
    return economic_bowlers
