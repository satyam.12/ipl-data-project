from collections import Counter
import matplotlib.pyplot as plt


def matches_played_per_year(matches):
    """compute the number of matches played in every year"""
    seasons = []
    for match in matches:
        seasons.append(match['season'])
    return dict(Counter(seasons))


def plot_matches_played_per_year(matches_per_year):
    """plot the number of matches played in every year"""
    plt.subplots(figsize=(10, 10))
    plt.bar(*zip(*sorted(matches_per_year.items())),color='indigo')
    plt.xlabel('Years')
    plt.ylabel('Number of Matches')
    plt.title("IPL Matches")
    plt.xticks(rotation=65)
    plt.savefig('matches_played_per_year.png')
    plt.show()


def compute_and_plot_matches_played_per_year(matches):
    """calling both compute and plot functions"""
    matches_per_year = matches_played_per_year(matches)
    plot_matches_played_per_year(matches_per_year)
